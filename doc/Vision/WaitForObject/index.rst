==========================================
Ждать появления / Wait for appearance
==========================================

Группа действий: *Машинное зрение / Vision*

________________________

Ждать появления изображения на экране. В случае совпадения переданного изображения с изображением(ями) на экране, возращаются координаты найденных совпадений. / Wait for the appearance of the given image on the display

--------------------
Иконка действия
--------------------

.. figure:: Doc/img/img.png
    :align: left
    :height: 50

|
|
|

--------------
Настройки
--------------

+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Свойство**   | **Англ. наименование** | **Описание**                                                                                | **Тип**          | **Пример заполнения** | **Обязательность**  |
|                |                        |                                                                                             |                  |                       | **заполнения поля** |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Параметры**                                                                                                                                                                                          |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Изображение    | Image                  | Путь до файла изображения.                                                                  | Robin.Image      | C:\\doc\\img.png      | Да                  |
|                |                        | Поддерживаемые форматы изображений: (jpeg, jpg, bmp, png, tif, tiff)                        |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Степень        | Similarity rate        | Коэффициент сходства между изображением и искомой областью.                                 | Robin.Numeric    | 1,34                  | Да                  |
| сходства       |                        | Максимальное значение равно "1"                                                             |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Возвращать     | Give center            | Если параметр равен "false", то возвращаются координаты верхнего левого угла изображения    | Robin.Boolean    | true                  | Нет                 |
| центр          |                        | найденного на экране, если "true" - возвращаются координаты центра найденного изображения   |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Таймаут, мс    | Timeout (ms)           | Время, в течении которого будет ожидаться появление заданного объекта.                      | Robin.Numeric    | 1,34                  | Да                  |
|                |                        | Таймаут задаётся в миллисекундах                                                            |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Результаты**                                                                                                                                                                                         |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Координаты     | Coordinates            | Координаты положения на экране где появилось совпадение                                     | Robin.Collection | Тулза в студии        |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+


