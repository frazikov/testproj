============================================
Найти страницу в PDF / Find page in PDF
============================================

Группа действий: *Машинное зрение / Vision*

________________________

Найти страницу в PDF / Find page in PDF

--------------------
Иконка действия
--------------------

.. figure:: Doc/img/img.png
    :align: left
    :height: 50

|
|
|

--------------
Настройки
--------------

+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Свойство**   | **Англ. наименование** | **Описание**                                                                                | **Тип**          | **Пример заполнения** | **Обязательность**  |
|                |                        |                                                                                             |                  |                       | **заполнения поля** |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Параметры**                                                                                                                                                                                          |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Путь к PDF     | PDF file path          | Путь к файлу PDF                                                                            | Robin.FilePath   | C:\\doc\\doc.pdf      | Да                  |
| файлу          |                        |                                                                                             |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Текст          | Text                   | Текст, который должна содержать искомая страница                                            | Robin.String     | Lorem Ipsum           | Да                  |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Ожидаемые      | Expected languages of  | Ожидаемые языки текста в PDF файле                                                          | Robin.String     | Lorem Ipsum           | Да                  |
| языки текста в | text in the PDF file   |                                                                                             |                  |                       |                     |
| PDF файле      |                        |                                                                                             |                  |                       |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| **Результаты**                                                                                                                                                                                         |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+
| Номера страниц | Page numbers           | Номера страниц, на которых был найден искомый текст                                         | Robin.Collection | Тулза в студии        |                     |
+----------------+------------------------+---------------------------------------------------------------------------------------------+------------------+-----------------------+---------------------+


